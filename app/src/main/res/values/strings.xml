<resources>
    <string name="app_name">Weather</string>

    <!---activity_settings.xml-->

    <string name="status">Status</string>
    <string name="logo">Logo</string>
    <string name="city">City</string>
    <string name="detect_location">Detect location</string>
    <string name="detect_location_automatic">Detect location automatically</string>
    <string name="detect_location_search">Getting your location…</string>
    <string name="detect_location_activate">Location is not enabled. Activate it to use the function.</string>
    <string name="detect_location_permission_not_granted">Location Permissions not granted.\nGrant it in the settings (GPS icon).</string>
    <string name="detect_location_new_location">New Location detected. Restart with new location…</string>
    <string name="api_key">API key (click for info)</string>
    <string name="enter_your_openweathermap_api_key">Enter your own API key</string>
    <string name="submit_data">Submit data</string>

    <!-- UI_Settings -->

    <string name="UiSettings">UI Settings</string>
    <string name="unit1">Units (Temp | Wind)</string>
    <string name="unit2">Units (Rain | Pressure)</string>
    <string name="chooseTheme">Theme:</string>
    <string name="gradient">Gradient</string>
    <string name="coloured_status_icons">Coloured status icons (Beta)</string>
    <string name="TimeFormat">24-hour format</string>
    <string name="ChooseLang">Choose Language</string>

    <string name="choose_city">Choose your city:</string>
    <string name="abort">Abort</string>

    <string name="error_occurred">An error occurred, old data will be loaded! You have probably reached the API limit or no connection…</string>
    <string name="error_1">The API key is not active or not specified!</string>
    <string name="error_2">The city is invalid or not specified!</string>
    <string name="error_3">Please check your internet connection!</string>

    <string name="reportError">Found a mistake? Strange behaviour?\nReport it on <a href="https://codeberg.org/BeoCode/Weather/issues">Codeberg</a> or via <a href="mailto:weather@beocode.eu">email</a></string>
    <string name="created_by">Created by <a href="https://codeberg.org/BeoCode/Weather">BeoCode</a> licensed under <a href="https://codeberg.org/BeoCode/Weather/src/branch/main/LICENSE">EUPL-1.2</a></string>
    <string name="OWM">Data provided by <a href="https://openweathermap.org/">OpenWeatherMap</a> licensed under <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA 4.0</a></string>
    <string name="Icons"><a href="https://erikflowers.github.io/weather-icons/">Icons</a> by <a href="https://www.twitter.com/erik_flowers">E. Flowers</a> &amp; <a href="https://www.twitter.com/artill">L. Bischoff</a> licensed under <a href="https://scripts.sil.org/OFL">SIL OFL 1.1</a></string>

    <!-- Conf_Widget -->

    <string name="configureWidget">Configure widget</string>
    <string name="widgetBackTrans">Make widget background transparent</string>
    <string name="ShowUpdatedAt">Don\'t show \"Updated:\"</string>
    <string name="highlightDay">Highlight day</string>
    <string name="widgetColour">Customize widget colour</string>
    <string name="red">Red</string>
    <string name="green">Green</string>
    <string name="blue">Blue</string>
    <string name="widgetUpdateInterval">Set widget update interval (for all widgets)</string>
    <string name="widgetUpdateIntervalUnit">min</string>
    <string name="widgetUpdateIntervalToShort">Note: Such a short update interval is not allowed for the standard key. The update interval has therefore been set to 30 minutes.</string>

    <!---activity_main.xml-->

    <string name="updated">Updated: </string>
    <string name="weatherMaps">Weather maps</string>
    <string name="feels">Feels like</string>
    <string name="weatherAlerts">There are weather alerts…</string>

    <string name="daytime">Daytime</string>
    <string name="sunrise">Sunrise</string>
    <string name="sunset">Sunset</string>

    <string name="wind">Wind</string>
    <string name="pressure">Pressure</string>
    <string name="humidity">Humidity</string>
    <string name="rain">Rain</string>
    <string name="rainCloud">Rain &amp; Cloudiness</string>
    <string name="rainRadar">Rain radar</string>
    <string name="snow">Snow</string>
    <string name="cloudiness">Cloudiness</string>
    <string name="visibility">Visibility</string>
    <string name="uvIndex">UV Index</string>
    <string name="windGust">Wind Gust</string>
    <string name="temperature">Temperature</string>
    <string name="Min">Min</string>
    <string name="Max">Max</string>

    <!---Fragments-->
    <string name="date">dd.MM.yyyy</string>
    <string name="daydate" translatable="false">E</string>
    <string name="daymonthdate">E, dd.MM</string>

    <string name="today">Today</string>
    <string name="tomorrow">Tomorrow</string>
    <string name="save">Save</string>
    <string name="add">Add</string>
    <string name="options">Options</string>
    <string name="switchCity">Swap %s with the main city (%s)</string>
    <string name="removeCity">Delete %s</string>

    <string name="alarm">Alarm</string>

    <string-array name="Themes">
        <item>System (Light/Dark)</item>
        <item>Light</item>
        <item>Dark</item>
        <item>Red</item>
        <item>Sand</item>
        <item>Blue</item>
    </string-array>

    <string-array name="Degree">
        <item>N</item>
        <item>NNE</item>
        <item>NE</item>
        <item>ENE</item>
        <item>E</item>
        <item>ESE</item>
        <item>SE</item>
        <item>SSE</item>
        <item>S</item>
        <item>SSW</item>
        <item>SW</item>
        <item>WSW</item>
        <item>W</item>
        <item>WNW</item>
        <item>NW</item>
        <item>NNW</item>
    </string-array>

    <string-array name="unitsTemp">
        <item>K</item>
        <item>°C</item>
        <item>°F</item>
    </string-array>

    <string-array name="unitsSpeed">
        <item>m/s</item>
        <item>km/h</item>
        <item>mph</item>
        <item>bft</item>
        <item>kn</item>
    </string-array>

    <string-array name="unitsDistance">
        <item>mm</item>
        <item>in</item>
    </string-array>

    <string-array name="unitsPressure">
        <item>hPa</item>
        <item>kPa</item>
        <item>mm Hg</item>
        <item>in Hg</item>
        <item>psi</item>
        <item>atm</item>
    </string-array>

    //Don't translate/add all Strings below to your translation
    <string name="time12" translatable="false">hh:mm a</string>
    <string name="time24" translatable="false">HH:mm</string>
    <string-array name="lang" translatable="false">
        <item>System</item>
        <item>English (100%)</item>
        <item>Armenian (70%)</item>
        <item>Bosnian (93%)</item>
        <item>Bulgarian (100%)</item>
        <item>Chinese (simpl.) (99%)</item>
        <item>Chinese (trad.) (100%)</item>
        <item>Czech (93%)</item>
        <item>Dutch (100%)</item>
        <item>Esperanto (93%)</item>
        <item>Finnish (93%)</item>
        <item>French (100%)</item>
        <item>Georgian (77%)</item>
        <item>German (100%)</item>
        <item>Greek (87%)</item>
        <item>Italian (99%)</item>
        <item>Macedonian (83%)</item>
        <item>Polish (100%)</item>
        <item>Portuguese (BR) (100%)</item>
        <item>Russian (93%)</item>
        <item>Serbian (Latin) (93%)</item>
        <item>Spanish (93%)</item>
        <item>Turkish (100%)</item>
        <item>Ukrainian (100%)</item>
    </string-array>
    <string name="standardKey" translatable="false">dc0bc479131e4d0b79b54e3a3e115ca0</string>
    <string-array name="lang_Code" translatable="false">
        <item>en</item>
        <item>hy</item>
        <item>bs</item>
        <item>bg</item>
        <item>zh</item>
        <item>zh_tw</item>
        <item>cz</item>
        <item>nl</item>
        <item>eo</item>
        <item>fi</item>
        <item>fr</item>
        <item>ka</item>
        <item>de</item>
        <item>el</item>
        <item>it</item>
        <item>mk</item>
        <item>pl</item>
        <item>pt_br</item>
        <item>ru</item>
        <item>sr</item>
        <item>es</item>
        <item>uk</item>
    </string-array>
</resources>
